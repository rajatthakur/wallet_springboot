package com.bootcamp.wallet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.sql.SQLOutput;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @Test
    void testControllerForGetWallet() throws Exception {
        when(walletService.getWalletForUser("Rajat")).thenReturn(new Wallet("Rajat", 10));

        mockMvc.perform(get("/wallet?username=Rajat"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"username\":\"Rajat\",\"balance\":10}"));
    }

    @Test
    void testControllerForUpdatedWalletBalance() throws Exception {
        when(walletService.addBalance(new WalletDTO("Rajat", 5))).thenReturn(new Wallet("Rajat", 10));
        mockMvc.perform(put("/addBalance")
                .content("{\"username\":\"Rajat\",\"credit\":5}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"username\":\"Rajat\",\"balance\":10}"));
    }

    @Test
    void testControllerForDeductWalletBalance() throws Exception {
        when(walletService.deductBalance(new WalletDTO("Rajat", 5))).thenReturn(new Wallet("Rajat", 2));
        mockMvc.perform(put("/deductBalance")
                .content("{\"username\":\"Rajat\",\"credit\":5}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
