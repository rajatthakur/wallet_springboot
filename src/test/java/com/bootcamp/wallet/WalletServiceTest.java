package com.bootcamp.wallet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class WalletServiceTest {
    private WalletService walletService;

    @Autowired
    private WalletRepository walletRepository;

    @Test
    void shouldReturnRightWalletForUser() throws Exception {
        walletService = new WalletService(walletRepository);
        assertEquals(new Wallet("Rajat", 11), walletService.addToRepository(new Wallet("Rajat", 11)));
        assertEquals(new Wallet("Rajat", 11), walletService.getWalletForUser("Rajat"));
    }

    @Test
    void shouldReturnUpdatedWallet() throws Exception {
        walletService = new WalletService(walletRepository);
        assertEquals(new Wallet("Rajat", 11), walletService.addToRepository(new Wallet("Rajat", 11)));
        assertEquals(new Wallet("Rajat", 111), walletService.addBalance(new WalletDTO("Rajat", 100)));
    }

    @Test
    void shouldReturnDeductWallet() throws Exception {
        walletService = new WalletService(walletRepository);
        assertEquals(new Wallet("Rajat", 11), walletService.addToRepository(new Wallet("Rajat", 11)));
        assertEquals(new Wallet("Rajat", 8), walletService.deductBalance(new WalletDTO("Rajat", 3)));
    }

}
