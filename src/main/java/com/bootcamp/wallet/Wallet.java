package com.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.Transaction;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Objects;

@Entity
class Wallet {
    @Id
    @GeneratedValue
    Long id;
    @JsonProperty
    @Size(min = 5, max = 12, message = "The size of the string must be between {min} and {max}.")
    private String username;

    private ArrayList<Transaction> transactions = new ArrayList<>();

    @JsonProperty
    @Range(min = 0, max = 10000, message = "The balance must be between {min} and {max}.")
    private int balance;

    String getUsername() {
        return username;
    }

    int getBalance() {
        return balance;
    }

    Wallet() {
    }

    Wallet(String username, int balance) {
        this.username = username;
        this.balance = balance;
    }

    String fetchUsername() {
        return this.username;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return balance == wallet.balance &&
                Objects.equals(username, wallet.username);
    }

}
