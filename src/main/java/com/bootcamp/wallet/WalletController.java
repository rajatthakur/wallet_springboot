package com.bootcamp.wallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
class WalletController {
    @Autowired
    private WalletService walletService;

    WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping("/wallet")
    Wallet wallet(@Valid @RequestParam("username") String username) {
        return walletService.getWalletForUser(username);
    }

    @PostMapping("/create")
    Wallet create(@Valid @RequestBody Wallet wallet) {
        return walletService.addToRepository(wallet);
    }

    @PutMapping("/addBalance")
    Wallet addBalance(@Valid @RequestBody WalletDTO walletDTO) throws UserNotRegisteredException {
        return walletService.addBalance(walletDTO);
    }

    @PutMapping("/deductBalance")
    Wallet deductBalance(@Valid @RequestBody WalletDTO walletDTO) throws UserNotRegisteredException {
        return walletService.deductBalance(walletDTO);
    }
}
