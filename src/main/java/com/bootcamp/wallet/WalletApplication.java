package com.bootcamp.wallet;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.LoggerFactory;

@Slf4j
@SpringBootApplication
public class WalletApplication {

	public static void main(String[] args) {
		Logger log = LoggerFactory.getLogger(WalletApplication.class);
		log.info("Hello!");
		SpringApplication.run(WalletApplication.class, args);
	}
}
