package com.bootcamp.wallet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long> {
    @Query("SELECT a FROM Wallet a WHERE a.username=:username")
    List<Wallet> fetchWallets(@Param("username") String username);
}