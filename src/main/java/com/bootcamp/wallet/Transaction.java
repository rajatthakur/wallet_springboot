package com.bootcamp.wallet;

import lombok.Getter;

enum TransactionType {
    DEBIT, CREDIT
}

@Getter
class Transaction {
    private Wallet wallet;
    private TransactionType transactionType;
    private double amount;

    Transaction(Wallet wallet, TransactionType transactionType, double amount) {
        this.wallet = wallet;
        this.transactionType = transactionType;
        this.amount = amount;
    }

}
