package com.bootcamp.wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
class WalletService {
    @Autowired
    private WalletRepository walletRepository;

    WalletService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    Wallet getWalletForUser(String username) {
        List<Wallet> walletList = this.walletRepository.fetchWallets(username);
        if (walletList == null || walletList.size() == 0) {
            return null;
        }
        return walletList.get(0);
    }

    Wallet addToRepository(Wallet wallet) {
        return this.walletRepository.save(wallet);
    }

    Wallet addBalance(WalletDTO walletDTO) throws UserNotRegisteredException {
        List<Wallet> walletList = this.walletRepository.fetchWallets(walletDTO.getUsername());
        if (walletList == null || walletList.size() == 0) {
            throw new UserNotRegisteredException("User is not registered.");
        }
        Wallet updatedWallet = new Wallet(walletList.get(0).getUsername(), walletList.get(0).getBalance() + walletDTO.getCredit());
        Transaction transaction = new Transaction(updatedWallet, TransactionType.DEBIT, walletDTO.getCredit());
        this.walletRepository.save(updatedWallet);
        return updatedWallet;
    }

    Wallet deductBalance(WalletDTO walletDTO) throws UserNotRegisteredException {
        List<Wallet> walletList = this.walletRepository.fetchWallets(walletDTO.getUsername());
        if (walletList == null || walletList.size() == 0) {
            throw new UserNotRegisteredException("User is not registered.");
        }
        if (walletList.get(0).getBalance() - walletDTO.getCredit() < 0){
            return new Wallet("Insufficient Funds", 0);
        }
        Wallet updatedWallet = new Wallet(walletList.get(0).getUsername(), walletList.get(0).getBalance() - walletDTO.getCredit());

        Transaction transaction = new Transaction(updatedWallet, TransactionType.CREDIT, walletDTO.getCredit());
        this.walletRepository.save(updatedWallet);
        return updatedWallet;
    }
}
