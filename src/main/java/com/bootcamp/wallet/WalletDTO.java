package com.bootcamp.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

class WalletDTO {
    @JsonProperty
    private String username;
    @JsonProperty
    private int credit;

    public WalletDTO(String username, int credit) {
        this.username = username;
        this.credit = credit;
    }

    String getUsername() {
        return username;
    }

    int getCredit() {
        return credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WalletDTO walletDTO = (WalletDTO) o;
        return credit == walletDTO.credit &&
                Objects.equals(username, walletDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, credit);
    }
}
