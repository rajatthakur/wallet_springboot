package com.bootcamp.wallet;

class UserNotRegisteredException extends Exception {
    UserNotRegisteredException(String message) {
        super(message);
    }
}
