create table wallet_transaction
(id bigint generated by default as identity,
wallet_id bigint,
amount integer not null,
type varchar(255),
remarks VARCHAR(255),
date TIMESTAMP default current_timestamp,
primary key (id));