FROM openjdk:11-jre
MAINTAINER Rajat Prince Thakur

ENV SPRING_PROFILE=dev

ADD build/libs/wallet-0.0.1-SNAPSHOT.jar /usr/share/wallet/wallet.jar
ENTRYPOINT ["java", "-jar", "/usr/share/wallet/wallet.jar"]